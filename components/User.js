import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { Avatar } from '@material-ui/core'
import { GridListTile } from '@material-ui/core'
import { GridListTileBar } from '@material-ui/core'
const useStyles = makeStyles((theme) => ({
  root: {
   
    marginBottom:'25px',
    marginRight:'5px',
    marginTop:'5px',
    borderRadius:'10px',
    height:'22vh',
    [theme.breakpoints.up('sm')]: {
    height: "25vh"
   },
    
    [theme.breakpoints.up('md')]: {
     display:'flex',
     justifyContent:'space-between',
     border:'none',
     boxShadow:'none',
     height:'8vh',
     marginBottom:'0px'
    
     
      
      
    },

   
  },
  action:{
    [theme.breakpoints.up('md')]: {
      padding:'2px',
      display: 'flex',
      justifyContent: "flex-end",
      alignItems: "center"
    }
  },
  content:{
    fontSize: "0.8rem",
    [theme.breakpoints.up('md')]: {
      fontSize: "1rem",
      display:'flex',
      padding:'2px',
     
      
      
    },
  },
  avatar:{
    marginLeft:'10px',
    marginRight:'8px',
    [theme.breakpoints.up('md')]: {
      marginLeft:'0px'
    }
  }
}))
const User = ({ name }) => {
  const classes = useStyles();
return(
  
  <GridListTile> 
  <Card className={classes.root } >
      
       <CardContent className={classes.content}>
         <Avatar style={{marginLeft:'10px',marginRight:'8px'}}/>
         
        
         <Typography style={{textAlign:'center'}} >
          {name}
        </Typography>
        </CardContent>
        
     <CardActions className={classes.action}>
      
        <Button size="small" variant='contained' color="primary" style={{borderRadius:'10px'}} >Follow</Button>
        </CardActions>
    </Card>
    </GridListTile>
    
   
  
)
}
export default User