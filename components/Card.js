import User from './User'
import {GridList} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {Typography} from '@material-ui/core'
import {Button} from '@material-ui/core'
const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
         overflow: 'hidden',
         padding:'20px',
         paddingTop:'0px',
         paddingBottom:'2px',
         [theme.breakpoints.up('md')]: {
         
          backgroundColor:'white'
          
        },
        
      
        
      },
      gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        [theme.breakpoints.up('md')]: {
         flexWrap:'wrap',
         display:'inline-block',
         height:'400px',
         width:'100%',
         marginRight:'0px'
          
          
        },
        
        
        
      },
      head:{
        display:'flex',
        justifyContent:'space-between',
        color:'white',
       padding:'20px',
        [theme.breakpoints.up('md')]: {
         backgroundColor:({back})=>back,
          
        },
      },
      content:{
        fontSize:'1rem',
        [theme.breakpoints.up('sm')]: {
          fontSize: "1.2rem",
          
          
        },
      },
      box:{
      margin:'20px 25px 20px 25px',
      backgroundColor:({back})=>back,
      border:'solid 1px black',
      [theme.breakpoints.up('md')]: {
        margin:'20px 40vh 20px 40vh',
        
        
      },
     
     
      

        
      },
      
  }))
const Card=({data,title,back, Child})=>{
    const classes = useStyles({back});
    return(
      
      <div className={classes.box} >
        
            <div className={classes.head}>
          <Typography variant='h5'className={classes.content}>{title}</Typography>
         <Button variant="contained" style={{borderRadius:'10px'}} size="small" >View All</Button></div>
          <div className={classes.root}>
      <GridList className={classes.gridList} cols={2.5} >
          {data.map((i, index)=>
        
            <Child key={index} {...i}/>
             
              
          )}
          </GridList>
          </div>
          
      
      </div>
        
        
    )
}
export default Card

