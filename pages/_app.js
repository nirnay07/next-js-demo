import { useState, useEffect } from "react"
// import { ThemeProvider } from "styled-components";
// import { lightTheme, darkTheme, GlobalStyles } from "../themeConfig"
// import theme from '../themes/base';

// import ThemeProvider from '../themeProvider/ThemeProvider';
import CssBaseline from '@material-ui/core/CssBaseline';
import Head from 'next/head';
// import { AuthProvider } from "../lib/Auth";




function MyApp({ Component, pageProps }) { 
     const [isMounted, setIsMounted] = useState(false)

     useEffect(() => {
       setIsMounted(true)
     }, [])

     useEffect(() => {
      // Remove the server-side injected CSS.
      const jssStyles = document.querySelector('#jss-server-side');
      if (jssStyles) {
        jssStyles.parentElement.removeChild(jssStyles);
      }
    }, []);
     
// const toggleTheme = () => {
//     theme == 'light' ? setTheme('dark') : setTheme('light')
// }
  return (
    <>
      <Head>
      <meta charSet="utf-8"/>
<meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
        <link rel='icon' href='/images/favicon.ico' type='image/x-icon' />
<meta name="application-name" content="Online community voting movies | Hoblist"/>
<meta name="robots" content="all" />
<meta name="googlebot" content="all" />
<meta name="author" content="Hoblist"/>

<meta name="theme-color" content="#5947ff"/>

<meta name="msapplication-navbutton-color" content="#5947ff"/>

<meta name="apple-mobile-web-app-status-bar-style" content="#5947ff"/>
<link rel="manifest" href="/manifest.json"/>
<link rel="icon" sizes="192x192" href="/images/192.png"/>
<link rel="apple-touch-icon" href="/images/192.png"/>
<meta name="msapplication-square310x310logo" content="/images/192.png"/>

  <meta name="description" content="Find the most popular favourites list in hoblist."/>
  <meta name="keywords" content="Top movies, best movies to watch, tranding movies"/>
  <title> Favourite List Suggestion-Hoblist</title>
  <meta property="og:description" content="Find the most popular favourites list in hoblist."/>
  <meta property="title" content="Favourite List Suggestion-Hoblist" />
  <meta property="og:title" content="Favourite List Suggestion-Hoblist" />
  <meta property="og:image" content="/images/logo-2.png"/>
  <meta property='og:site_name' content='Hoblist' />
  <meta property='og:type' content="video.movie" />
      </Head>

      
        <CssBaseline />
        {/* <div style={{float: 'right'}}>
      <button onClick={toggleTheme}>{theme}</button>
      </div> */}
        {isMounted && (
          
            <Component {...pageProps} />
         
        )}
    
    </>
  ); 
}



export default MyApp
